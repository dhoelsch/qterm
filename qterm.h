#ifndef QTERM_H
#define QTERM_H

#include <QMainWindow>

namespace Ui {
class QTerm;
}

class QTerm : public QMainWindow
{
    Q_OBJECT

public:
    explicit QTerm(QWidget *parent = 0);
    ~QTerm();

private slots:

  void on_actionQuit_triggered();

  void on_actionAbout_triggered();

private:
    Ui::QTerm *ui;
};

#endif // QTERM_H
