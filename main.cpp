#include "qterm.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTerm w;
    w.show();

    return a.exec();
}
