#include "qterm.h"
#include "ui_qterm.h"

QTerm::QTerm(QWidget *parent)
 :QMainWindow(parent), ui(new Ui::QTerm) {
    ui->setupUi(this);
}

QTerm::~QTerm() {
    delete ui;
}

void QTerm::on_actionQuit_triggered()
{
     qApp->quit();
}

void QTerm::on_actionAbout_triggered()
{

}
